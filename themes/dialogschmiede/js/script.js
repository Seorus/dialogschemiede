(function ($, Drupal, settings) {

    "use strict";

    Drupal.behaviors.general = {
        attach: function (context) {

            var bannerPage = $(".banner", context);

            bannerPage.each(function () {

                var imgUrl = $(this).find('img').attr('src');

                $(this).attr('data-image-src', imgUrl);

                $(this).find('img').remove();

            });

            $(".hamburger", context).click(function () {
                $(this).toggleClass("is-active");
                $("#block-mainnavigation .menu").toggle();
            });

            $('.map-google-wrap').click(function () {
                $(this).find('iframe').addClass('clicked')
            })
                .mouseleave(function () {
                    $(this).find('iframe').removeClass('clicked')
                });


            var myParaxify = paraxify('.paraxify', {
                speed: 1,
                boost: 0
            });

        }
    };

})(jQuery, Drupal, drupalSettings);